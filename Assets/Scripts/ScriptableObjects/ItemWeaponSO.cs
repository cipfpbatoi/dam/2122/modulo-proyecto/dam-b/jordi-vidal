using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewItemWeapon", menuName = "Scriptable Objects/Inventory/Item Weapon")]
public class ItemWeaponSO : ItemSO
{
    public float attackPower;
}
