using System.Collections;
using System.Collections.Generic;
using ScriptableObjectArchitecture;
using UnityEngine;

public class DialogueTrigger : MonoBehaviour
{
    [Header("Configuration")] 
    public ConversationSO conversation;

    [Header("Broadcasting events")] 
    public ConversationSOGameEvent conversationRequestEvent;

    public void TriggerConversation()
    {
        conversationRequestEvent.Raise(conversation);
    }
}
