using ScriptableObjectArchitecture;
using UnityEngine;

public class PlayerInteraction : MonoBehaviour
{
    [Header("Configuration")] 
    public string interactableTag;

    [Header("Broadcasting events")]
    public BoolGameEvent interactionRequestEvent;

    private Interactable _interactable;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag(interactableTag))
        {
            var interactable = collision.GetComponent<Interactable>();
            _interactable = interactable;
        }
        
        interactionRequestEvent.Raise((_interactable != null));
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag(interactableTag))
        {
            _interactable = null;
        }
        
        interactionRequestEvent.Raise((_interactable != null));
    }

    public void EnableInteractable()
    {
        if (_interactable != null)
        {
            _interactable.Interact();
        }
    }
}
